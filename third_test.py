from selenium import webdriver
import unittest
from selenium.webdriver.common.keys import Keys
import time


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://www.google.com/')

    # find input field, enter the 'Python' word and press 'Enter' button
    def test_01(self):
        driver = self.driver
        input_field = driver.find_element_by_name('q')
        input_field.send_keys('python')
        input_field.send_keys(Keys.ENTER)

        # test comment
        time.sleep(3)
        titles = driver.find_elements_by_class_name('LC20lb DKV0Md')
        for title in titles:
            assert 'python' in title.text.lower()

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
